# 海康解码推流

#### 项目介绍

##### 使用海康SDK获取视频流，Emgucv处理后通过FFmpeg管道推流。

- byte推流
```
ffmpeg -y -f rawvideo -vcodec rawvideo -pix_fmt bgr24 -s 640x480 -r 10 -i - -c:v libx264 -pix_fmt yuv420p -preset ultrafast -f flv rtmp://120.26.82.157:1935/live/mystream
```

- 图片推流
```c#
 Process proc = new Process();

            proc.StartInfo.FileName = @"ffmpeg.exe";
            proc.StartInfo.Arguments = String.Format("-f image2pipe -i pipe:.bmp -maxrate {0}k -r {1} -an -y {2}",
                bitrate, fps, outputfilename);
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.RedirectStandardInput = true;
            proc.StartInfo.RedirectStandardOutput = true;

            proc.Start();

            for (int i = 0; i < 500; i++)
            {
                using (var ms = new MemoryStream())
                {
                    using (var img = Image.FromFile(@"lena.png"))
                    {
                        img.Save(ms, System.Drawing.Imaging.ImageFormat.Bmp);
                        ms.WriteTo(proc.StandardInput.BaseStream);
                    }
                }
            }
```

- 推流解决方案

> https://stackoverflow.com/questions/19658415/how-to-use-pipe-in-ffmpeg-within-c-sharp
> https://mathewsachin.github.io/blog/2017/07/28/ffmpeg-pipe-csharp.html
> https://jiasule.v2ex.com/t/436280

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)