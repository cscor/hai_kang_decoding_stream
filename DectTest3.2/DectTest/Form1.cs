﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using System.IO;

namespace DECPROC
{
    public partial class Form1 : Form
    {
        private string[] files;
        private int filesIndex = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            //正常清晰图片 440, 58, 107,607 ->.\pic5\0801235908.jpg
            Image<Bgr, Byte> img = new Image<Bgr, byte>(@"1500312950.jpg");//.\pic8\100031-4076.jpg.\pic8\1530316166.jpg .\pic8\113031-5529.jpg .\pic8\1100328790.jpg 1500312950.jpg
            //img.ROI = new Rectangle(573, 131, 95, 576);
            ImageProc(img, new Rectangle(573, 131, 95, 576));
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //中午图片脏
            Image<Bgr, Byte> img = new Image<Bgr, byte>(@".\pic2\BQ000128161207090912.jpg"); //.\pic2\BQ000128161207090912.jpg .\pic2\BQ000128161207092412.jpg
            //img.ROI = new Rectangle(987, 33, 106, 1037);
            ImageProc(img, new Rectangle(987, 33, 106, 1037));
        }

        private void button3_Click(object sender, EventArgs e)
        {
            //早上图片
            Image<Bgr, Byte> img = new Image<Bgr, byte>(@".\pic2\BQ000128161207083912.jpg");//.\BQ\BQ000128161207112412.jpg .\BQ\BQ000128161207172413.jpg BQ000128161207083912 15003129502.jpg BQ000128161207090912
            //img.ROI = new Rectangle(987, 33, 106, 1037);
            ImageProc(img, new Rectangle(987, 33, 106, 1037));
        }

        private void button4_Click(object sender, EventArgs e)
        {
            //晚上图片
            Image<Bgr, Byte> img = new Image<Bgr, byte>(@".\BQ\BQ000128161207173913.jpg");//15003129503.jpg .\BQ\BQ000128161207173913
            //img.ROI = new Rectangle(987, 33, 106, 1037);
            ImageProc(img, new Rectangle(987, 33, 106, 1037));
        }

        private void button5_Click(object sender, EventArgs e)
        {
            //晚上图片2
            Image<Bgr, Byte> img = new Image<Bgr, byte>("003113-6777.jpg");
            //img.ROI = new Rectangle(577, 73, 108, 643);
            ImageProc(img, new Rectangle(577, 73, 108, 643));
        }

        private void button6_Click(object sender, EventArgs e)
        {
            //中午图片脏2
            Image<Bgr, Byte> img = new Image<Bgr, byte>("070141-4036.jpg"); //0731408670 0631415222 070141-4036 160137-5336 123136-3563.jpg 100113-9545 1631386950 070141-4036
            //img.ROI = new Rectangle(577, 73, 108, 643);
            ImageProc(img, new Rectangle(607, 76, 57, 733));
        }

        private void button7_Click(object sender, EventArgs e)
        {
            //傍晚图片
            Image<Bgr, Byte> img = new Image<Bgr, byte>(@".\pic2\BQ000128161207063912.jpg");
            //img.ROI = new Rectangle(987, 33, 108, 1037);
            ImageProc(img, new Rectangle(607, 76, 57, 733));
        }

        //其他图片查看
        private void button8_Click(object sender, EventArgs e)
        {
            files = Directory.GetFiles(".\\pic19");

            //Timer tm = new Timer();

            //this.BeginInvoke(new Action<string>((a) => { MessageBox.Show(a); }), "1");

            Image<Bgr, Byte> img = new Image<Bgr, byte>(files[filesIndex]);
            //img.ROI = new Rectangle 7->(591, 73, 88, 643); 8->(573, 131, 95, 576)
            //5->440,58,107,607 10、11->574,137,100,580、4->597, 76, 79, 593   3、17->591, 73, 88, 643
            //13、14、19->604, 75, 63, 651  15、16、18->578, 127, 93, 590
            ImageProc(img, new Rectangle(604, 75, 63, 651));
            filesIndex++;
            if (filesIndex == files.Length)
                filesIndex = 0;
        }

        //Oth
        private void button13_Click(object sender, EventArgs e)
        {
            //Image<Bgr, Byte> img = new Image<Bgr, byte>(@".\pic19\1001286502.jpg");//.\pic13\073120-6780.jpg.\pic14\110058-8151.jpg.\pic14\133105-3626.jpg .\pic16\1030120614.jpg
            //img.ROI = new Rectangle(591, 73, 88, 643); 8->573, 131, 95, 576; 10、11->574, 137, 100, 580；4->597, 76, 79, 593
            //.\pic4\150115-8817.jpg 15->578, 127, 93, 590

            //Image<Bgr, Byte> img = new Image<Bgr, byte>("BQ000121170109193347.jpg");//BQ000121170111183337 BQ000121170110053348 BQ000121170109193347.jpg BQ000121170109230346.jpg BQ000121170109183347.jpg
            //ImageProc(img, new Rectangle(988, 10, 71, 1134));

            //Image<Bgr, Byte> img = new Image<Bgr, byte>("BQ000128170114180402.jpg"); //BQ000128170114180402 BQ000128170109193351 BQ000128170110003350 BQ000128170112070341 BQ000128170115033403 BQ000128170110070349 BQ000128170110063349 BQ000128170109190351
            //ImageProc(img, new Rectangle(961, 25, 47, 1143));

            //Image<Bgr, Byte> img = new Image<Bgr, byte>("BQ000116170112190328.jpg");//BQ000116170112190328 BQ000116170112233328 BQ000116170110180338 BQ000116170109183343
            //ImageProc(img, new Rectangle(811, 21, 73, 1165));

            Image<Bgr, Byte> img = new Image<Bgr, byte>("filetest.jpg"); //
            ImageProc(img, new Rectangle(485, 76, 57, 268));
            
        }

        private void button10_Click(object sender, EventArgs e)
        {
            //平武山洪不锈钢贴面
            Image<Bgr, Byte> img = new Image<Bgr, byte>("平武山洪不锈钢贴面.jpg");
            //img.ROI = new Rectangle(718, 417, 652, 2788);
            ImageProc(img, new Rectangle(718, 417, 652, 2788));
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Image<Bgr, Byte> img = new Image<Bgr, byte>("上虞山洪桥墩贴面.jpg");
            //img.ROI = new Rectangle(396, 181, 124, 601);
            ImageProc(img, new Rectangle(420, 181, 40, 601));
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Image<Bgr, Byte> img = new Image<Bgr, byte>("永定山洪镀锌管立柱.jpg");
            //img.ROI = new Rectangle(183, 114, 30, 471);
            ImageProc(img, new Rectangle(183, 114, 30, 471));
        }

        //2其他图片查看
        private void button12_Click(object sender, EventArgs e)
        {
            files = Directory.GetFiles(".\\pic2");//pic2 BQ

            Image<Bgr, Byte> img = new Image<Bgr, byte>(files[filesIndex]);
            //img.ROI = ;
            ImageProc(img, new Rectangle(987, 33, 106, 1037));
            filesIndex++;
            if (filesIndex == files.Length)
                filesIndex = 0;
        }

        private void ImageProc(Image<Bgr, Byte> img, Rectangle roi)
        {
            using (img)
            {
                using (DetectRequest DtRequest = new DetectRequest(img, roi)
                {
                    IsProc = false,
                    WaterRulerTopPoint = new Point(roi.Width * 1 / 2, 5) //水尺左上角第一个点
                })
                {
                    DetectProcesser dp0 = new DetectProcesser_0();
                    DetectProcesser dp1 = new DetectProcesser_1();
                    DetectProcesser dp1_1 = new DetectProcesser_1_1();
                    DetectProcesser dp2 = new DetectProcesser_2();
                    DetectProcesser dp3 = new DetectProcesser_3();
                    DetectProcesser dp4 = new DetectProcesser_4();
                    DetectProcesser dp5 = new DetectProcesser_5();
                    DetectProcesser dp7 = new DetectProcesser_7();

                    //dp5.NextProcesser = dp0;
                    //dp0.NextProcesser = dp1;
                    //dp1.NextProcesser = dp2;
                    //dp2.NextProcesser = dp3;
                    //dp3.NextProcesser = dp4;

                    //dp5.NextProcesser = dp1; //晚上补光情况
                    //dp1.NextProcesser = dp0; //早上光线不均匀图片
                    //dp0.NextProcesser = dp2; //通用检测
                    //dp2.NextProcesser = dp4; //晚上没补光情况
                    //dp4.NextProcesser = dp3; //倒影情况
                    //dp3 晚上图片补充

                    //dp5.NextProcesser = dp0;
                    //dp0.NextProcesser = dp1;
                    //dp1.NextProcesser = dp3;
                    ////dp2.NextProcesser = dp3;
                    //dp3.NextProcesser = dp4;


                    dp7.ProcessRequest(DtRequest);

                    IbImg.Image = DtRequest.SrcImg;
                    IbGray.Image = DtRequest.Gray;
                    IbDst.Image = DtRequest.GrayToDstDisplay;
                    IbResult.Image = DtRequest.Reslut;
                }
            }
        }

        private void ImageProc1(Image<Bgr, Byte> img)
        {
            //HSV颜色分离
            var tempImg = new Mat();
            CvInvoke.CvtColor(img, tempImg, ColorConversion.Bgr2Hsv);
            var imgs = tempImg.Split();

            Image<Gray, Byte> gray = imgs[2].ToImage<Gray, byte>();

            CvInvoke.PutText(img, "M 01", new Point(1, 20), FontFace.HersheyTriplex, 0.5, new MCvScalar(0, 255, 0));
            CvInvoke.PutText(img, "Threshold", new Point(1, 35), FontFace.HersheyTriplex, 0.5, new MCvScalar(0, 255, 255));
            CvInvoke.PutText(img, "Balance", new Point(1, 50), FontFace.HersheyTriplex, 0.5, new MCvScalar(0, 255, 255));
            CvInvoke.PutText(img, "Close", new Point(1, 65), FontFace.HersheySimplex, 0.5, new MCvScalar(0, 255, 255));
            CvInvoke.PutText(img, "Open", new Point(1, 80), FontFace.HersheyTriplex, 0.5, new MCvScalar(0, 255, 255));

            Mat dst = new Mat(gray.Size, DepthType.Cv8U, 1);
            Mat dst1 = new Mat(gray.Size, DepthType.Cv8U, 1);
            Mat dst2 = new Mat(gray.Size, DepthType.Cv8U, 1);

            //白平衡
            //CvInvoke.BalanceWhite(gray.Mat, dst, Emgu.CV.CvEnum.WhiteBalanceMethod.Simple, 0, 255, 0, 255);

            CvInvoke.Threshold(dst, dst, 35, 255, ThresholdType.ToZero);

            //形态学 闭操作 连接一些连通域，也就是删除一些目标区域的白色的洞
            Mat elemenCanny = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(3, 3), new Point(0, 0)); //15,15早上图片
            CvInvoke.MorphologyEx(dst, dst, MorphOp.Close, elemenCanny, new Point(0, 0), 1, BorderType.Default, new MCvScalar());  //方法1  方法3 方法4

            Mat elemenCanny1 = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(1, 1), new Point(0, 0));
            CvInvoke.MorphologyEx(dst, dst, MorphOp.Open, elemenCanny1, new Point(0, 0), 1, BorderType.Default, new MCvScalar()); //方法3


            dst.CopyTo(dst1);


            VectorOfVectorOfPoint vvp = new VectorOfVectorOfPoint();
            CvInvoke.FindContours(dst, vvp, null, RetrType.External, ChainApproxMethod.ChainApproxSimple);

            for (int i = 0; i < vvp.Size; i++)
            {
                using (VectorOfPoint contour = vvp[i])
                {
                    Rectangle rect = CvInvoke.BoundingRectangle(contour);
                    int tmparea = rect.Height * rect.Width;

                    if (tmparea > 1000)// && rect.Height / rect.Width > 1.5)
                    {
                        CvInvoke.Rectangle(img, new Rectangle(rect.X, rect.Y, rect.Width, rect.Height), new MCvScalar(0, 255, 0), 2);
                    }
                }
            }

            Mat result = new Mat(dst.Size, Emgu.CV.CvEnum.DepthType.Cv8U, 3);
            CvInvoke.DrawContours(result, vvp, -1, new MCvScalar(0, 255, 0));
            
            dst.Dispose();

            //CvInvoke.NamedWindow(win1, NamedWindowType.AutoSize);

            IbImg.Image = img;
            IbGray.Image = gray;
            IbDst.Image = dst1;
            IbResult.Image = result;
        }

        private void ImageProcTemp(Image<Bgr, Byte> img)
        {
            //img = img.SmoothGaussian(5);//高斯模糊处理 去噪

            //Image<Gray, Byte> GrayImg = new Image<Gray, Byte>(img.Width, img.Height);
            //CvInvoke.CvtColor(img, GrayImg, Emgu.CV.CvEnum.ColorConversion.Rgb2Gray); //转灰度

            //HSV颜色分离
            var tempImg = new Mat();
            CvInvoke.CvtColor(img, tempImg, ColorConversion.Bgr2Hsv);
            var imgs = tempImg.Split();
            //CvInvoke.Imshow("h", imgs[0]);
            //CvInvoke.Imshow("s", imgs[1]);
            //CvInvoke.Imshow("v", imgs[2]);

            Image<Gray, Byte> gray = imgs[2].ToImage<Gray, byte>();

            //Image<Gray, Byte> gray = img.Mat.ToImage<Gray, byte>();
            Mat dst = new Mat(gray.Size, DepthType.Cv8U, 1);
            Mat dst1 = new Mat(gray.Size, DepthType.Cv8U, 1);
            Mat dst2 = new Mat(gray.Size, DepthType.Cv8U, 1);

            //HSV 颜色空间分类

            //中值滤波 去噪
            //CvInvoke.MedianBlur(gray, gray, 1);

            //img = img.SmoothGaussian(5);//高斯模糊处理 去噪

            //双变滤波
            //img = img.SmoothBilatral(10, 30, 15);

            //锐化
            //int[, ,] data = new int[,,] { { { -1 }, { -1 }, { -1 } }, { { -1 }, { 9 }, { -1 } }, { { -1 }, { -1 }, { -1 } } };
            //Image<Gray, int> kennel = new Image<Gray, int>(data);
            //Image<Gray, float> kernelI = new Image<Gray, float>(3, 3, new Gray(0));
            //kernelI.Data[1, 1, 0] = (float)5.0;
            //kernelI.Data[0, 1, 0] = (float)-1.0;
            //kernelI.Data[1, 0, 0] = (float)-1.0;
            //kernelI.Data[2, 1, 0] = (float)-1.0;
            //kernelI.Data[1, 2, 0] = (float)-1.0;
            //CvInvoke.Filter2D(dst, dst, kernelI, new Point(-1, -1));

            //gray._GammaCorrect(5);//5     

            //白平衡
            //CvInvoke.BalanceWhite(gray.Mat, dst, Emgu.CV.CvEnum.WhiteBalanceMethod.Simple, 0, 255, 0, 255);

            //均衡化 增加对比度
            //CvInvoke.EqualizeHist(dst, dst); //晚上图片

            //int Blue_threshold = 50; //0-255
            //int Green_threshold = 50; //0-255
            //int Red_threshold = 50; //0-255
            //img = img.ThresholdBinary(
            //  new Bgr(Blue_threshold, Green_threshold, Red_threshold), new Bgr(255, 255, 255));

            //阈值处理

            CvInvoke.Threshold(dst, dst, 35, 255, ThresholdType.ToZero);//35 方法1 方法3

            //CvInvoke.Threshold(dst, dst, 35, 255, ThresholdType.Binary); //方法1

            //CvInvoke.Threshold(dst, dst, 120, 255, ThresholdType.Binary); //方法2

            //CvInvoke.Imshow("dst2", dst2);

            //CvInvoke.Threshold(dst, dst, 120, 255, ThresholdType.Binary);  //晚上图片
            //CvInvoke.AdaptiveThreshold(gray, dst, 255, AdaptiveThresholdType.MeanC, ThresholdType.Binary, 35, -5); //22
            //CvInvoke.AdaptiveThreshold(gray, dst, 255, Emgu.CV.CvEnum.AdaptiveThresholdType.MeanC, Emgu.CV.CvEnum.ThresholdType.Binary, 35, 1);
            //Emgu.CV.CvEnum.ThresholdType.Binary, 101, -10);

            //腐蚀
            dst = dst.ToImage<Gray, byte>().Erode(2).Mat; //方法4

            //边缘检测 canny
            int edgeThresh = 35;
            //CvInvoke.Canny(dst, dst, edgeThresh, edgeThresh * 3); //方法3 方法4
            //CvInvoke.Sobel(gray, dst, DepthType.Cv8U, 0, 1, 3);
            //CvInvoke.Sobel(dst, dst, Emgu.CV.CvEnum.DepthType.Default, 1, 0);
            //CvInvoke.Laplacian(dst, dst, Emgu.CV.CvEnum.DepthType.Default, 3);


            //膨胀
            dst = dst.ToImage<Gray, byte>().Dilate(1).Mat; //方法4

            //开运算 删除一些零零星星的噪点
            //Mat elemenCanny1 = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(3, 3), new Point(0, 0));
            //CvInvoke.MorphologyEx(dst, dst, MorphOp.Open, elemenCanny1, new Point(0, 0), 1, BorderType.Default, new MCvScalar()); //方法1

            //形态学 闭操作 连接一些连通域，也就是删除一些目标区域的白色的洞
            Mat elemenCanny = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(3, 3), new Point(0, 0)); //15,15早上图片
            CvInvoke.MorphologyEx(dst, dst, MorphOp.Close, elemenCanny, new Point(0, 0), 1, BorderType.Default, new MCvScalar());  //方法1  方法3 方法4

            //Mat elemenCanny1 = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(1, 1), new Point(0, 0));
            //CvInvoke.MorphologyEx(dst, dst, MorphOp.Open, elemenCanny1, new Point(0, 0), 1, BorderType.Default, new MCvScalar()); //方法3

            //CvInvoke.Threshold(dst, dst, 75, 255, ThresholdType.ToZero);
            //for (int i = 0; i < 5; i++)
            //{
            //    CvInvoke.EqualizeHist(dst, dst); //晚上图片
            //}
            //CvInvoke.Threshold(dst, dst, 35, 255, ThresholdType.ToZero);

            dst.CopyTo(dst1);

            VectorOfVectorOfPoint vvp = new VectorOfVectorOfPoint();
            CvInvoke.FindContours(dst, vvp, null, RetrType.External, ChainApproxMethod.ChainApproxSimple);

            for (int i = 0; i < vvp.Size; i++)
            {
                using (VectorOfPoint contour = vvp[i])
                {
                    Rectangle rect = CvInvoke.BoundingRectangle(contour);
                    int tmparea = rect.Height * rect.Width;

                    if (tmparea > 1000)// && rect.Height / rect.Width > 1.5)
                    {
                        CvInvoke.Rectangle(img, new Rectangle(rect.X, rect.Y, rect.Width, rect.Height), new MCvScalar(0, 255, 0), 2);
                    }
                }
            }

            Mat result = new Mat(dst.Size, Emgu.CV.CvEnum.DepthType.Cv8U, 3);
            CvInvoke.DrawContours(result, vvp, -1, new MCvScalar(0, 255, 0));

            dst.Dispose();

            //CvInvoke.NamedWindow(win1, NamedWindowType.AutoSize);

            IbImg.Image = img;
            IbGray.Image = gray;
            IbDst.Image = dst1;
            IbResult.Image = result;

            //CvInvoke.cvResetImageROI(img);
            //CvInvoke.Imshow("img", img.Copy());

            //CvInvoke.Imshow("img", img);
            //CvInvoke.NamedWindow("img", NamedWindowType.Normal);
            //CvInvoke.Imshow("img", img); //Show the image
            //CvInvoke.Imshow("gray", gray); //Show the image
            //CvInvoke.Imshow("dst", dst); //Show the image
            //CvInvoke.NamedWindow("result", NamedWindowType.Normal);
            //CvInvoke.Imshow("result", result); //Show the image

            //GrayImg._GammaCorrect(3);//5     

            //创建一个7*7大小的矩形内核
            //Mat elemenCanny = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(7, 7), new Point(0, 0));
            //形态学闭运算
            //CvInvoke.MorphologyEx(GrayImg, GrayImg, MorphOp.Close, elemenCanny, new Point(0, 0), 1, BorderType.Default, new MCvScalar());
            //形态学开运算
            //CvInvoke.MorphologyEx(GrayImg, GrayImg, MorphOp.Open, elemenCanny, new Point(0, 0), 1, BorderType.Default, new MCvScalar());

            //var threshImage = GrayImg.CopyBlank();

            //CvInvoke.Threshold(GrayImg,threshImage,0,255,ThresholdType.Otsu);

            //ROI
            //threshImage.ROI = new Rectangle(987, 33, 96, 1000);

            //CvInvoke.FindContours(GrayImg, contours, null, RetrType.External, ChainApproxMethod.ChainApproxSimple);
            //CvInvoke.DrawContours(GrayImg, contours, -1, new MCvScalar(0, 0, 255));

            //var imgROI = new Mat(img.Mat, new Rectangle(987, 33, 96,1000 ));
            //CvInvoke.Imshow("ROI", imgROI);//ROI图像  
        }

    }
}