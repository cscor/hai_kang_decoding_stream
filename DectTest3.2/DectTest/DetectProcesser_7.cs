﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using System.Drawing;
using System.Windows.Forms;

namespace DECPROC
{
    /// <summary>
    /// 应用场景：倒影处理，主要实现：局部自适应阈值化，象素邻域大小43，均值-43
    /// </summary>
    public class DetectProcesser_7 : DetectProcesser
    {
        //主要用于倒影处理
        public override void ProcessRequest(DetectRequest Request)
        {
            //HSV颜色分离
            Mat imgHSV = new Mat();
            CvInvoke.CvtColor(Request.SrcImg, imgHSV, ColorConversion.Bgr2Hsv);
            //imgHSV = Request.SrcImg.Mat.ToImage<Hsv, byte>().Mat; //颜色空间转换
            VectorOfMat hsvSplit = new VectorOfMat(imgHSV.Split());
            CvInvoke.EqualizeHist(hsvSplit[2], hsvSplit[2]);
            CvInvoke.Merge(hsvSplit, imgHSV);

            //桔色
            Mat imgOrangeThresholded = new Mat();
            CvInvoke.InRange(imgHSV, new ScalarArray(new MCvScalar(0, 43, 46)), 
                new ScalarArray(new MCvScalar(12, 255, 255)), imgOrangeThresholded); 

            //红色
            Mat imgRedThresholded = new Mat();
            CvInvoke.InRange(imgHSV, new ScalarArray(new MCvScalar(120, 43, 46)),  
                new ScalarArray(new MCvScalar(180, 255, 255)), imgRedThresholded); 

            //黄色
            Mat imgYellowThresholded = new Mat();
            CvInvoke.InRange(imgHSV, new ScalarArray(new MCvScalar(35, 43, 46)),  
                new ScalarArray(new MCvScalar(50, 255, 255)), imgYellowThresholded); 

            CvInvoke.BitwiseOr(imgRedThresholded, imgOrangeThresholded, imgRedThresholded);

            CvInvoke.BitwiseOr(imgRedThresholded, imgYellowThresholded, Request.GrayToDst);

            //Request.Gray = imgOrangeThresholded.ToImage<Gray, byte>();

            //边缘检测
            //int edgeThresh = 90; //67
            //Mat imgCanny = new Mat();
            //CvInvoke.Canny(Request.Gray, imgCanny, edgeThresh, edgeThresh * 3);

            //System.Drawing.Size mSizeC = new System.Drawing.Size(5, 5);
            //Mat elemenCannyC = CvInvoke.GetStructuringElement(ElementShape.Rectangle, mSizeC, new Point(0, 0));
            //CvInvoke.MorphologyEx(imgCanny, imgCanny, MorphOp.Close, elemenCannyC, new Point(0, 0), 1, BorderType.Default, new MCvScalar());

            //CvInvoke.BitwiseOr(imgRedThresholded, imgCanny, Request.GrayToDst);

            //Request.Gray = imgCanny.ToImage<Gray, byte>();

            //System.Drawing.Size mSizeR = new System.Drawing.Size(3, 3);
            //Mat elemenCannyR = CvInvoke.GetStructuringElement(ElementShape.Rectangle, mSizeR, new Point(0, 0));
            //CvInvoke.MorphologyEx(imgRedThresholded, imgRedThresholded, MorphOp.Close, elemenCannyR, new Point(0, 0), 1, BorderType.Default, new MCvScalar());

            //黄色
            //Mat imgYellowThresholded = new Mat();
            //CvInvoke.InRange(imgHSV, new ScalarArray(new MCvScalar(22, 43, 46)),
            //    new ScalarArray(new MCvScalar(38, 255, 255)), imgYellowThresholded); //Threshold the image

            ////imgYellowThresholded = imgYellowThresholded.ToImage<Gray, byte>().Erode(1).Mat;
            //System.Drawing.Size mSizeYel = new System.Drawing.Size(3, 3);
            //Mat elemenCannyYel = CvInvoke.GetStructuringElement(ElementShape.Rectangle, mSizeYel, new Point(0, 0));
            //CvInvoke.MorphologyEx(imgYellowThresholded, imgYellowThresholded, MorphOp.Open, elemenCannyYel, new Point(0, 0), 1, BorderType.Default, new MCvScalar());

            //System.Drawing.Size mSizeYel1 = new System.Drawing.Size(3, 3);
            //Mat elemenCannyYel1 = CvInvoke.GetStructuringElement(ElementShape.Rectangle, mSizeYel1, new Point(0, 0));
            //CvInvoke.MorphologyEx(imgYellowThresholded, imgYellowThresholded, MorphOp.Close, elemenCannyYel1, new Point(0, 0), 1, BorderType.Default, new MCvScalar());

            //CvInvoke.AdaptiveThreshold(Request.Gray, Request.Gray, 255, AdaptiveThresholdType.MeanC, ThresholdType.Binary, 33, -1);

            //Request.Gray = imgRedThresholded.ToImage<Gray, byte>();

            //CvInvoke.BitwiseOr(imgRedThresholded, imgYellowThresholded, imgRedThresholded);

            //CvInvoke.BitwiseOr(imgRedThresholded, Request.GrayToDst, Request.GrayToDst);

            //Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Erode(1).Mat; //方法4

            //Mat imgAdaThresholded = new Mat();
            //CvInvoke.AdaptiveThreshold(Request.Gray, imgAdaThresholded, 255, AdaptiveThresholdType.MeanC, ThresholdType.Binary, 33, -3);
            
            //System.Drawing.Size mSize = new System.Drawing.Size(9, 9);
            //Mat elemenCanny = CvInvoke.GetStructuringElement(ElementShape.Rectangle, mSize, new Point(0, 0));
            //CvInvoke.MorphologyEx(imgRedThresholded, imgRedThresholded, MorphOp.Close, elemenCanny, new Point(0, 0), 2, BorderType.Default, new MCvScalar());

            //CvInvoke.BitwiseOr(imgRedThresholded, imgYellowThresholded, imgYellowThresholded);

            //Mat imgOtsuThresholded = new Mat();
            //CvInvoke.Threshold(Request.Gray, imgOtsuThresholded, 0, 255, ThresholdType.Otsu); //120

            //Request.Gray = imgOtsuThresholded.ToImage<Gray, byte>();

            //Request.Gray = imgAdaThresholded.ToImage<Gray, byte>();

            //CvInvoke.BitwiseAnd(imgAdaThresholded, imgOtsuThresholded, Request.GrayToDst);

            //CvInvoke.BitwiseOr(Request.GrayToDst, imgRedThresholded, Request.GrayToDst);

            //System.Drawing.Size mSize2 = new System.Drawing.Size(2, 2);
            //Mat elemenCanny2 = CvInvoke.GetStructuringElement(ElementShape.Rectangle, mSize2, new Point(0, 0));
            //CvInvoke.MorphologyEx(Request.GrayToDst, Request.GrayToDst, MorphOp.Open, elemenCanny2, new Point(0, 0), 1, BorderType.Default, new MCvScalar());

            Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Dilate(1).Mat;

            System.Drawing.Size mSize1 = new System.Drawing.Size(5, 5);
            Mat elemenCanny1 = CvInvoke.GetStructuringElement(ElementShape.Rectangle, mSize1, new Point(0, 0));
            CvInvoke.MorphologyEx(Request.GrayToDst, Request.GrayToDst, MorphOp.Close, elemenCanny1, new Point(0, 0), 1, BorderType.Default, new MCvScalar());

            //CvInvoke.BitwiseOr(imgRedThresholded, Request.GrayToDst, Request.GrayToDst);

            //划线：时和有上角连接起来，形成连通域。
            CvInvoke.Line(Request.GrayToDst, new Point(0, 0), Request.WaterRulerTopPoint, new MCvScalar(255, 255, 255), 2);

            /*
            CvInvoke.AdaptiveThreshold(Request.Gray, Request.GrayToDst, 255, AdaptiveThresholdType.MeanC, ThresholdType.Binary, 33, -1); //22 -33 -13

            //腐蚀
            //Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Erode(1).Mat; //方法4

            System.Drawing.Size mSize = new System.Drawing.Size(2, 2);

            Mat elemenCanny = CvInvoke.GetStructuringElement(ElementShape.Rectangle, mSize, new Point(0, 0)); //15,15早上图片

            //划线：时和有上角连接起来，形成连通域。
            CvInvoke.Line(Request.GrayToDst, new Point(0, 0), Request.WaterRulerTopPoint, new MCvScalar(255, 255, 255), 2);
            */

            FindContoursENextProc(Request);
        }

        public override string ToString()
        {
            return "";//|Erode|AdaptiveThreshold|Close|Line
        }
    }
}
