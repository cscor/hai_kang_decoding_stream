﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using System.Drawing;
using System.Windows.Forms;

namespace DECPROC
{
    //晚上补光，光线强烈并且有倒影情况下
    /// <summary>
    /// 应用场景：晚上补光情况，主要实现：局部自适应阈值化，根据对比度做调整
    /// </summary>
    public class DetectProcesser_5 : DetectProcesser
    {
        public override void ProcessRequest(DetectRequest Request)
        {
            //base.ProcessRequest(Request);

            if (!Request.BSaveMatchImages && Request.v0 > 140 && Request.v0 < 150 && Request.sdv > 65 && Request.sdv < 70 && null != NextProcesser)
            {
                NextProcesser.ProcessRequest(Request);
                return;
            }

            if (Request.BSaveMatchImages || ((Request.v0 > 160 && Request.sdv > 50) ||
                (Request.v0 > 150 && Request.sdv > 50 && Request.sdv < 57) ||
                (Request.v0 > 130 && Request.v0 < 135 && Request.sdv > 53 && Request.sdv < 54.5) ||
                (Request.v0 > 170 && Request.sdv > 40)
                ))
            {
                //CvInvoke.BalanceWhite(Request.Gray.Mat, Request.GrayToDst, Emgu.CV.CvEnum.WhiteBalanceMethod.Simple, 0, 255, 0, 255);

                //CvInvoke.EqualizeHist(Request.Gray, Request.GrayToDst);

                //double mParam = Request.v0 > 135 ? -3 : -33; // 135  p16
                //mParam = Request.v0 < 150 && Request.sdv > 55 ? -23 : mParam;
                //mParam = Request.v0 > 135 && Request.v0 < 140 ? -23 : mParam;
                double mParam = Request.sdv > 45 && Request.v0 > 145 ? -3 : -33;
                CvInvoke.AdaptiveThreshold(Request.Gray, Request.GrayToDst, 255, AdaptiveThresholdType.MeanC, ThresholdType.Binary, 55, mParam); //22

                //Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Erode(1).Mat;

                //CvInvoke.Threshold(Request.GrayToDst, Request.GrayToDst, 35, 255, ThresholdType.ToZero);

                Mat elemenCanny = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(7, 7), new Point(0, 0)); //15,15早上图片
                CvInvoke.MorphologyEx(Request.GrayToDst, Request.GrayToDst, MorphOp.Close, elemenCanny, new Point(0, 0), 2, BorderType.Default, new MCvScalar());  //方法1  方法3 方法4

                Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Erode(3).Mat;

                //划线：时和有上角连接起来，形成连通域。
                CvInvoke.Line(Request.GrayToDst, new Point(0, 0), Request.WaterRulerTopPoint, new MCvScalar(255, 255, 255), 2);

                FindContoursENextProc(Request);
            }
            else
            {
                if (null != NextProcesser)
                    NextProcesser.ProcessRequest(Request);
                return;
            }
        }

        public override string ToString()
        {
            return "Proc_5_FillNight";//|AdaptiveThreshold|Erode
        }
    }
}
