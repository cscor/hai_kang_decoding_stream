﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;
using System.Drawing;
using System.Windows.Forms;

namespace DECPROC
{
    /// <summary>
    /// 应用场景：倒影处理，主要实现：局部自适应阈值化，象素邻域大小43，均值-43
    /// </summary>
    public class DetectProcesser_4 : DetectProcesser
    {
        //主要用于倒影处理
        public override void ProcessRequest(DetectRequest Request)
        {
            //base.ProcessRequest(Request);

            //太暗、太亮不检测 97
            if (!Request.BSaveMatchImages && (Request.v0 < 80 || Request.v0 > 160) && null != NextProcesser)
            {
                NextProcesser.ProcessRequest(Request);
                return;
            }

            //腐蚀
            Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Erode(1).Mat; //方法4

            //CvInvoke.Threshold(Request.GrayToDst, Request.GrayToDst, 35, 255, ThresholdType.ToZero);

            //double mParam = Request.v0 < 100 ? 0 : -19;

            CvInvoke.AdaptiveThreshold(Request.Gray, Request.GrayToDst, 255, AdaptiveThresholdType.MeanC, ThresholdType.Binary, 43, -43); //22 -33 -13

            //MessageBox.Show(Request.Gray.GetAverage().MCvScalar.V0.ToString());
            System.Drawing.Size mSize = new System.Drawing.Size(8, 8);

            //else if (Request.v0 < 115 || Request.v0 > 120)
            //{
            //    mSize = new System.Drawing.Size(5, 5);
            //}

            //if (Request.v0 > 120 && Request.v0 < 130)
            //{
            //    mSize = new System.Drawing.Size(7,7);
            //}

            Mat elemenCanny = CvInvoke.GetStructuringElement(ElementShape.Rectangle, mSize, new Point(0, 0)); //15,15早上图片
            CvInvoke.MorphologyEx(Request.GrayToDst, Request.GrayToDst, MorphOp.Close, elemenCanny, new Point(0, 0), 1, BorderType.Default, new MCvScalar());  //方法1  方法3 方法4

            Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Erode(1).Mat; 

            //if (Request.v0 > 120)
            //    Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Erode(1).Mat;

            //划线：时和有上角连接起来，形成连通域。
            CvInvoke.Line(Request.GrayToDst, new Point(0, 0), Request.WaterRulerTopPoint, new MCvScalar(255, 255, 255), 2);

            FindContoursENextProc(Request);
        }

        public override string ToString()
        {
            return "Proc_4_Reflections";//|Erode|AdaptiveThreshold|Close|Line
        }
    }
}
