﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using System.Drawing;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.IO;

namespace DECPROC
{
    public static class WaterRulerProcesser
    {
        public static Rectangle OutImageSize { get; set; }

        //每种算法都执行一遍并保存图片
        public static void ProcessAll(Image<Bgr, Byte> img, Rectangle roi, RulerParam rulerParam, string dirPath, out Dictionary<string,int> mDcMatchProc)
        {
            MatchImages matchImages = new MatchImages();
            Dictionary<string, int> tmpDc = new Dictionary<string, int>();

            List<DetectProcesser> dpList = new List<DetectProcesser>{ 
                new DetectProcesser_0(),
                new DetectProcesser_1(),
                new DetectProcesser_2(),
                new DetectProcesser_3(),
                new DetectProcesser_4(),
                new DetectProcesser_5(),
                new DetectProcesser_6()
            };

            dpList.ForEach(dp => {
                using (DetectRequest DtRequest = new DetectRequest(img.Copy(), roi)
                {
                    IsProc = false,
                    BSaveMatchImages = true,
                    WaterRulerTopPoint = new Point(roi.Width * 2 / 3, rulerParam.Para1 + 3) //水尺左上角第一个点
                })
                {
                    if (DtRequest.BSaveMatchImages)
                    {
                        dp.ProcessRequest(DtRequest);

                        int waterActualLevel = 0;

                        if (DtRequest.IsProc)
                        {
                            //水尺坐标转换实际刻度
                            waterActualLevel = CalcWaterLevel(rulerParam, DtRequest.WaterRulerBottom);

                            DtRequest.SrcImg.ROI = Rectangle.Empty;

                            string mkey = string.Format("{0}-{1}-{2}-{3}", dp.ToString(), Math.Round(DtRequest.avg, 2), Math.Round(DtRequest.sdv), waterActualLevel);
                            CvInvoke.PutText(DtRequest.SrcImg, "water level:" + Convert.ToString(waterActualLevel) + "cm", new Point(1, 35), FontFace.HersheySimplex, 1, new MCvScalar(0, 255, 255));

                            //matchImages[mkey] = DtRequest.SrcImg;

                            //保存图片
                            if (!Directory.Exists(dirPath))
                            {
                                Directory.CreateDirectory(dirPath);
                            }

                            string filePath = dirPath.TrimEnd('\\') + @"\" + mkey + ".jpg";
                            if (!File.Exists(filePath))
                            {
                                //图片压缩
                                DtRequest.SrcImg = DtRequest.SrcImg.Resize(OutImageSize.Width, OutImageSize.Height, Inter.Area);
                                DtRequest.SrcImg.Save(filePath);

                                //生成小图片
                                string thumbDir = dirPath.TrimEnd('\\') + @"\thumb";
                                string thumbPath = thumbDir.TrimEnd('\\') + @"\" + mkey + ".jpg";
                                if (!Directory.Exists(thumbDir))
                                {
                                    Directory.CreateDirectory(thumbDir);
                                }
                                DtRequest.SrcImg = DtRequest.SrcImg.Resize(200, 115, Inter.Area);
                                DtRequest.SrcImg.Save(thumbPath);
                            }

                            tmpDc.Add(dp.ToString(), waterActualLevel);
                        }
                    }
                }
            });

            mDcMatchProc = tmpDc;

            //if (!Directory.Exists(dirPath))
            //{
            //    Directory.CreateDirectory(dirPath);
            //    matchImages.Save(dirPath);
            //}

            //matchImages.DcMatchImages.Keys.ToList<string>().ForEach(d =>
            //    {

            //    });
        }

        //选择最优算法
        public static int Process(Image<Bgr, Byte> img, Rectangle roi, RulerParam rulerParam, string dirPath, ref string imgPath,out bool isProc)
        {
            //识别水位坐标
            using (DetectRequest DtRequest = new DetectRequest(img, roi)
            {
                IsProc = false,
                BSaveMatchImages = false,
                WaterRulerTopPoint = new Point(roi.Width / 2, rulerParam.Para1 + 10) //水尺左上角第一个点
            })
            {
                DetectProcesser dp0 = new DetectProcesser_0();
                DetectProcesser dp1 = new DetectProcesser_1();
                DetectProcesser dp2 = new DetectProcesser_2();
                DetectProcesser dp3 = new DetectProcesser_3();
                DetectProcesser dp4 = new DetectProcesser_4();
                DetectProcesser dp5 = new DetectProcesser_5();

                //dp5.NextProcesser = dp1; //晚上补光情况
                //dp1.NextProcesser = dp0; //早上光线不均匀图片
                //dp0.NextProcesser = dp2; //通用检测
                //dp2.NextProcesser = dp4; //晚上没补光情况
                //dp4.NextProcesser = dp3; //倒影情况
                //dp3 晚上图片补充

                dp5.NextProcesser = dp0;
                dp0.NextProcesser = dp1;
                dp1.NextProcesser = dp3;
                //dp2.NextProcesser = dp3;
                dp3.NextProcesser = dp2; //dp4

                dp5.ProcessRequest(DtRequest);

                isProc = DtRequest.IsProc;

                int waterActualLevel = 0;

                if (isProc)
                {
                    //水尺坐标转换实际刻度
                    waterActualLevel = CalcWaterLevel(rulerParam, DtRequest.WaterRulerBottom);

                    //CvInvoke.PutText(DtRequest.SrcImg, "water level:", new Point(1, 15), FontFace.HersheyTriplex, 0.5, new MCvScalar(255, 0, 255));
                    //CvInvoke.PutText(DtRequest.SrcImg, Convert.ToString(waterActualLevel) + " cm", new Point(1, 25), FontFace.HersheyTriplex, 0.5, new MCvScalar(0, 0, 255));

                    //取消ROI
                    DtRequest.SrcImg.ROI = Rectangle.Empty;
                    //CvInvoke.cvResetImageROI(DtRequest.SrcImg);

                    CvInvoke.PutText(DtRequest.SrcImg, "water level:" + Convert.ToString(waterActualLevel) + "cm", new Point(1, 35), FontFace.HersheySimplex, 1, new MCvScalar(0, 255, 255));

                    //CvInvoke.PutText(DtRequest.SrcImg, "water level:" + Convert.ToString(waterActualLevel) + " cm", new Point(roi.X + 1, roi.Y - 5), FontFace.HersheySimplex, 0.5, new MCvScalar(255, 0, 255));
                    if (!Directory.Exists(dirPath))
                        Directory.CreateDirectory(dirPath);
                    imgPath = string.Format(@"{0}\{1}-{2}-{3}.jpg", dirPath, imgPath, DtRequest.ProcName, waterActualLevel);
                    //图片压缩
                    img = img.Resize(OutImageSize.Width, OutImageSize.Height, Inter.Area);
                    img.Save(imgPath);
                }
                return waterActualLevel;
            }
        }

        //水尺坐标转换实际刻度
        public static int CalcWaterLevel(RulerParam rulerParam, int waterRulerBottom)
        {
            int rulerLen = rulerParam.RulerLen;
            int waterLevel;

            if (waterRulerBottom < rulerParam.Para1)
            {
                waterLevel = rulerLen;
            }
            else if (waterRulerBottom < rulerParam.Para2)
            {
                waterLevel = rulerLen * 2 / 3 + (int)((rulerParam.Para2 - waterRulerBottom) * (rulerLen / 3) / (rulerParam.Para2 - rulerParam.Para1));
            }
            else if (waterRulerBottom < rulerParam.Para3)
            {
                waterLevel = rulerLen / 3 + (int)((rulerParam.Para3 - waterRulerBottom) * (rulerLen / 3) / (rulerParam.Para3 - rulerParam.Para2));
            }
            else if (waterRulerBottom < rulerParam.Para4)
            {
                waterLevel = (int)((rulerParam.Para4 - waterRulerBottom) * (rulerLen / 3) / (rulerParam.Para4 - rulerParam.Para3));
            }
            else
            {
                waterLevel = 0;
            }
            if (waterLevel < 0)
            {
                waterLevel = 0;
            }
            return waterLevel;
        }
    }
}
