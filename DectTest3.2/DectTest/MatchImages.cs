﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using System.IO;

namespace DECPROC
{
    public class MatchImages
    {
        public Dictionary<string, Image<Bgr, byte>> DcMatchImages { get; set; }

        public MatchImages()
        {
            DcMatchImages = new Dictionary<string, Image<Bgr, byte>>();
        }

        public Image<Bgr, byte> this[string name]
        {
            get {
                return DcMatchImages[name];
            }

            set {
                DcMatchImages.Add(name, value);
            }
        }

        public void Save(string dirPath)
        {
            if (string.IsNullOrEmpty(dirPath)) return;

            foreach (KeyValuePair<string, Image<Bgr, byte>> im in DcMatchImages)
            {
                string filePath = dirPath.TrimEnd('\\') + @"\" + im.Key + ".jpg";
                if (!File.Exists(filePath))
                    im.Value.Save(filePath);
            }
        }
    }
}