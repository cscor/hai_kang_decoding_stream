﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using Emgu.CV.Util;

namespace DECPROC
{
    /// <summary>
    /// 应用场景：通用检测，主要实现：全局阈值化，阈值取值根据对比度调整
    /// </summary>
    public class DetectProcesser_1 : DetectProcesser
    {
        public override void ProcessRequest(DetectRequest Request)
        {
            //base.ProcessRequest(Request);

            //太暗不检测
            if ((Request.v0 < 63 || Request.v0 > 160 ||
                Request.sdv < 30) && null != NextProcesser)
            {
                NextProcesser.ProcessRequest(Request);
                return;
            }

            //白平衡
            //CvInvoke.BalanceWhite(Request.Gray.Mat, Request.GrayToDst, Emgu.CV.CvEnum.WhiteBalanceMethod.Simple, 0, 255, 0, 255);
            CvInvoke.EqualizeHist(Request.Gray, Request.GrayToDst);

            //阈值
            //int threshold = Request.v0 > 90 ? 60 : 35; //90 亮度判断 水尺未干情况
            int threshold = (int)Request.sdv;
            threshold += threshold > 45 ? 27 : 10;
            //threshold = threshold < 60 ? threshold + (int)Request.avg : (int)Request.avg;
            //threshold = Request.v0 > 90 && Request.sdv > 50 ? 35 : threshold;
            CvInvoke.Threshold(Request.GrayToDst, Request.GrayToDst, threshold, 255, ThresholdType.ToZero);
            //CvInvoke.Threshold(Request.GrayToDst, Request.GrayToDst, 0, 255, ThresholdType.Otsu);

            Request.GrayToDst = Request.GrayToDst.ToImage<Gray, byte>().Dilate(3).Mat;

            //int edgeThresh = 35;
            //CvInvoke.Canny(Request.GrayToDst, Request.GrayToDst, edgeThresh, edgeThresh * 3); //方法3 方法4

            //形态学 闭操作 连接一些连通域，也就是删除一些目标区域的白色的洞
            Mat elemenCanny = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(10, 10), new Point(0, 0)); //15,15早上图片
            CvInvoke.MorphologyEx(Request.GrayToDst, Request.GrayToDst, MorphOp.Close, elemenCanny, new Point(0, 0), 1, BorderType.Default, new MCvScalar());  //方法1  方法3 方法4

            //开
            //Mat elemenCanny1 = CvInvoke.GetStructuringElement(ElementShape.Rectangle, new System.Drawing.Size(1, 1), new Point(0, 0));
            //CvInvoke.MorphologyEx(Request.GrayToDst, Request.GrayToDst, MorphOp.Open, elemenCanny1, new Point(0, 0), 1, BorderType.Default, new MCvScalar()); //方法3


            FindContoursENextProc(Request);
        }

        public override string ToString()
        {
            return "Proc_1_Normal";//|BalanceWhite|Threshold|Close|Open
        }
    }
}
