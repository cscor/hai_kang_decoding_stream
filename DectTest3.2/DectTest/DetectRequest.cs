﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Emgu.CV;
using Emgu.CV.Structure;
using Emgu.CV.CvEnum;
using System.Drawing;
using Emgu.CV.Util;

namespace DECPROC
{
    public class DetectRequest : IDisposable
    {

        public double v0;
        public double avg;
        public double sdv;

        public bool IsProc { get; set; }

        public Image<Bgr, Byte> Img { get; set; }

        public Image<Bgr, Byte> SrcImg { get; set; }

        public Image<Gray, Byte> Gray { get; set; }

        public Mat GrayToDst { get; set; }

        public Mat GrayToDstDisplay { get; set; }

        public Mat Reslut { get; set; }

        public Rectangle ROIRectangle { get; set; }

        public Point WaterRulerTopPoint { get; set; }

        public int WaterRulerBottom { get; set; }

        public bool BSaveMatchImages { get; set; }

        public string ProcName { get; set; }

        public DetectRequest(Image<Bgr, Byte> src, Rectangle roi)
        {
            //if (null != this.SrcImg)
            //{
            //设置ROI
            this.SrcImg = src;
            this.ROIRectangle = roi;
            this.SrcImg.ROI = this.ROIRectangle;

            //HSV颜色分离
            //Mat tempImg = new Mat();
            //CvInvoke.CvtColor(this.SrcImg, tempImg, ColorConversion.Bgr2Hsv);

            //this.Gray = this.SrcImg.Mat.ToImage<Gray, byte>();

            this.Gray = this.SrcImg.Split()[2];

            v0 = this.Gray.GetAverage().MCvScalar.V0;

            MCvScalar msdv = new MCvScalar();
            Gray gavg = new Gray();
            this.Gray.AvgSdv(out gavg, out msdv);
            avg = gavg.MCvScalar.V0;
            sdv = msdv.V0;
            //MessageBox.Show("_v0:" + _v0 + "\n avg:" + avg.MCvScalar.V0 + "\n sdv:" + sdv.V0);

            //var imgs = tempImg.Split();
            //Request.Gray = imgs[2].ToImage<Gray, byte>();

            //MessageBox.Show(Request.Gray.GetAverage().MCvScalar.V0.ToString());

            //CvInvoke.Imshow("h", imgs[0]);
            //CvInvoke.Imshow("s", imgs[1]);
            //CvInvoke.Imshow("v", imgs[2]);

            this.GrayToDst = new Mat(this.Gray.Size, DepthType.Cv8U, 1);
            this.GrayToDstDisplay = new Mat(this.Gray.Size, DepthType.Cv8U, 1);
            //Request.Reslut = new Mat(Request.GrayToDst.Size, Emgu.CV.CvEnum.DepthType.Cv8U, 3);
            //}
        }

        public void Dispose()
        {
            SrcImg.Dispose();
            Gray.Dispose();
            GrayToDst.Dispose();
            GrayToDstDisplay.Dispose();
            if (Reslut != null)
                Reslut.Dispose();
        }
    }
}