﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace HCEmguCvDemo
{
    public class VideoPusher
    {
        Process proc = new Process();

        private VideoPusher()
        {
            var commandStr = @"-y -f rawvideo -vcodec rawvideo -pix_fmt bgr24 -s 640x480 -r 10 -i - -c:v libx264 -pix_fmt yuv420p -preset ultrafast -f flv rtmp://120.26.82.157:1935/live/mystream";
            //var commandStr = @"-y -f rawvideo -vcodec rawvideo -pix_fmt bgr24 -s 640x480 -use_wallclock_as_timestamps 1 -i - -c:v libx264 -pix_fmt yuv420p -preset ultrafast -f flv rtmp://120.26.82.157:1935/live/mystream";
            //var commandStr = @"-y -f rawvideo -vcodec rawvideo -pix_fmt bgr24 -s 640x480 -re -i - -c:v libx264 -pix_fmt yuv420p -preset slow -f flv rtmp://120.26.82.157:1935/live/mystream";
            //var commandStr = @"-y -f rawvideo -vcodec rawvideo -pix_fmt bgr24 -s 640x480 -i - -c:v libx264 -pix_fmt yuv420p -preset ultrafast -f flv rtmp://120.26.82.157:1935/live/mystream";

            var inputArgs = "-framerate 20 -f rawvideo -pix_fmt rgb32 -video_size 640x480 -i -";
            var outputArgs = "-vcodec libx264 -crf 23 -pix_fmt yuv420p -preset ultrafast -r 20 -f flv rtmp://120.26.82.157:1935/live/mystream";

            proc.StartInfo.FileName = @"ffmpeg.exe";
            proc.StartInfo.Arguments = commandStr; //string.Format("{0} {1}", inputArgs, outputArgs); 
            proc.StartInfo.UseShellExecute = false;
            proc.StartInfo.CreateNoWindow = true;
            proc.StartInfo.RedirectStandardInput = true;
            proc.StartInfo.RedirectStandardOutput = true;

            proc.Start();
        }

        public void Push(byte[] bytes)
        {
            proc.StandardInput.BaseStream.Write(bytes, 0, bytes.Length);
        }

        public static VideoPusher Instance
        {
            get
            {
                return VideoPusherNested.instance;
            }
        }

        class VideoPusherNested
        {
            internal static readonly VideoPusher instance = new VideoPusher();
            static VideoPusherNested() { }
        }
    }
}
