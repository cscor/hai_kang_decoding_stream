﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace HCEmguCvDemo
{
    public class StationInfo
    {
        public string Use { get; set; }

        /// <summary>
        /// 是否启用
        /// </summary>
        public bool IsUse
        {
            get
            {
                return "1" == Use.Trim();
            }
        }

        public string ID { get; set; }

        public string Name { get; set; }

        public string RulerLen { get; set; }

        public int RulerLength
        {
            get
            {
                return Convert.ToInt32(RulerLen);
            }
        }

        public string SrcDir { get; set; }

        public string SaveDir { get; set; }

        public PARA Param { get; set; }

        public ROI Roi { get; set; }

        public int WaterLevl { get; set; }
    }


    public class PARA
    {
        public string P1 { get; set; }

        public string P2 { get; set; }

        public string P3 { get; set; }

        public string P4 { get; set; }

        public int Para1
        {
            get
            {
                return Convert.ToInt32(P1);
            }
        }

        public int Para2
        {
            get
            {
                return Convert.ToInt32(P2);
            }
        }

        public int Para3
        {
            get
            {
                return Convert.ToInt32(P3);
            }
        }

        public int Para4
        {
            get
            {
                return Convert.ToInt32(P4);
            }
        }
    }


    public class ROI
    {
        public string X { get; set; }

        public string Y { get; set; }

        public string Width { get; set; }

        public string Height { get; set; }

        public int RoiX
        {
            get
            {
                return Convert.ToInt32(X);
            }
        }

        public int RoiY
        {
            get
            {
                return Convert.ToInt32(Y);
            }
        }

        public int RoiWidth
        {
            get
            {
                return Convert.ToInt32(Width);
            }
        }

        public int RoiHeight
        {
            get
            {
                return Convert.ToInt32(Height);
            }
        }

    }
}
