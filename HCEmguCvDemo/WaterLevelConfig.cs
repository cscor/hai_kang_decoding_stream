﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Drawing;
using System;

namespace HCEmguCvDemo
{
    public class WaterLevelConfig
    {

        private static string xmlPath = Directory.GetCurrentDirectory() + @"\config.xml";

        public static string DbType { get; set; }
        
        public static string DbConnstr { get; set; }

        public static Rectangle InImageSize { get; set; }

        public static Rectangle OutImageSize { get; set; }

        public static bool IsSaveAllProcImages
        {
            get
            {
                return (!string.IsNullOrEmpty(SaveAllProcsImages) && SaveAllProcsImages == "1");
            }
        }

        public static string SaveAllProcsImages { get; set; }

        public static List<StationInfo> StationList { get; set; }

        public static void Load()
        {
            if (!File.Exists(xmlPath))
            {
                XDocument xdoc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"),
                    new XElement("waterlevel",
                        new XElement("dbconnstr",
                            new XAttribute("dbtype", "SQL"),
                            new XAttribute("dbconn", "Initial Catalog=ACQDB;Data Source=120.26.82.157;user ID=strong;Password=strong9889")),
                        new XElement("save_all_procs_images","0"),
                        new XElement("inimage",
                                new XAttribute("width",1280),
                                new XAttribute("height", 720)),

                        new XElement("outimage",
                                new XAttribute("width", 640),
                                new XAttribute("height", 360)),
                        new XElement("station",
                            new XElement("use", "1"),
                            new XElement("id", "1"),
                            new XElement("name", "新站点"),
                            new XElement("rulerLen","300"),
                            new XElement("srcdir"),
                            new XElement("savedir"),
                            new XElement("param",
                                new XAttribute("para1", ""),
                                new XAttribute("para2", ""),
                                new XAttribute("para3", ""),
                                new XAttribute("para4", "")),
                            new XElement("roi",
                                new XAttribute("x", "0"),
                                new XAttribute("y", "0"),
                                new XAttribute("width", "0"),
                                new XAttribute("height", "0"))
                            )
                    ));
                xdoc.Save(xmlPath);
            }

            XDocument xdocument = XDocument.Load(xmlPath);

            var dbconnstr = xdocument.Element("waterlevel").Element("dbconnstr");
            DbType = dbconnstr.Attribute("dbtype").Value;
            DbConnstr = dbconnstr.Attribute("dbconn").Value;

            SaveAllProcsImages = xdocument.Descendants("save_all_procs_images").FirstOrDefault().Value;
            var xInImage = xdocument.Descendants("inimage").FirstOrDefault();
            InImageSize = new Rectangle()
            {
                Width = Convert.ToInt32(xInImage.Attribute("width").Value),
                Height = Convert.ToInt32(xInImage.Attribute("height").Value),
            };

            var xOutImage = xdocument.Descendants("outimage").FirstOrDefault();
            OutImageSize = new Rectangle()
            {
                Width = Convert.ToInt32(xOutImage.Attribute("width").Value),
                Height = Convert.ToInt32(xOutImage.Attribute("height").Value),
            };

            var st = from x in xdocument.Descendants("station")
                     select new StationInfo
                     {
                         Use = x.Element("use").Value,
                         ID = x.Element("id").Value,
                         Name = x.Element("name").Value,
                         RulerLen = x.Element("rulerLen").Value,
                         SrcDir = x.Element("srcdir").Value,
                         SaveDir = x.Element("savedir").Value,
                         Param = new PARA
                         {
                             P1 = x.Element("param").Attribute("para1").Value,
                             P2 = x.Element("param").Attribute("para2").Value,
                             P3 = x.Element("param").Attribute("para3").Value,
                             P4 = x.Element("param").Attribute("para4").Value
                         },
                         Roi = new ROI
                         {
                             X = x.Element("roi").Attribute("x").Value,
                             Y = x.Element("roi").Attribute("y").Value,
                             Width = x.Element("roi").Attribute("width").Value,
                             Height = x.Element("roi").Attribute("height").Value
                         }
                     };
            StationList = st.ToList();
        }

        public static void AddNewStation()
        {
            WaterLevelConfig.StationList.Add(new StationInfo
                {
                    Use = "0",
                    ID = "new",
                    Name = "新站点",
                    RulerLen = "300",
                    SaveDir = @"D:\SCImage\Result",
                    Param = new PARA(),
                    Roi = new ROI
                    {
                        X = "0",
                        Y = "0",
                        Width = "0",
                        Height = "0"
                    }
                });

            //WaterLevelConfig.Stations.Concat<StationInfo>(new StationInfo[]
            //{
            //    new StationInfo
            //    {
            //        ID = "new",
            //        Name = "新站点",
            //        RulerLen = "300",
            //        SaveDir = @"D:\SCImage\Result",
            //        Param = new PARA(),
            //        Roi = new ROI() 
            //    }
            //});
        }

        public static void Save()
        {
            XDocument xdoc = new XDocument(new XDeclaration("1.0", "utf-8", "yes"),
                    new XElement("waterlevel",
                        new XElement("dbconnstr",
                                new XAttribute("dbtype", DbType),
                                new XAttribute("dbconn", DbConnstr)),

                        new XElement("save_all_procs_images", SaveAllProcsImages),

                        new XElement("inimage",
                                new XAttribute("width", InImageSize.Width),
                                new XAttribute("height", InImageSize.Height)),

                        new XElement("outimage",
                                new XAttribute("width", OutImageSize.Width),
                                new XAttribute("height", OutImageSize.Height)),

                        from st in StationList
                        select new XElement("station",
                                    new XElement("use", st.Use),
                                    new XElement("id", st.ID),
                                    new XElement("name", st.Name),
                                    new XElement("rulerLen",st.RulerLen),
                                    new XElement("srcdir", st.SrcDir),
                                    new XElement("savedir", st.SaveDir),
                                    new XElement("param",
                                        new XAttribute("para1", st.Param.P1),
                                        new XAttribute("para2", st.Param.P2),
                                        new XAttribute("para3", st.Param.P3),
                                        new XAttribute("para4", st.Param.P4)),
                                    new XElement("roi",
                                        new XAttribute("x", st.Roi.X),
                                        new XAttribute("y", st.Roi.Y),
                                        new XAttribute("width", st.Roi.Width),
                                        new XAttribute("height", st.Roi.Height))
                                        )
                   ));

            xdoc.Save(xmlPath);
        }
    }
}
